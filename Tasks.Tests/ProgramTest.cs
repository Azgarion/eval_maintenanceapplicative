﻿using System;
using System.IO;
using NUnit.Framework;
using Should;

namespace Tasks
{
    [TestFixture]
    public class ProgramTest
    {
        private StringWriter output;

        [SetUp]
        public void Initialize()
        {
            this.output = new StringWriter();
            Console.SetOut(output);
        }
        
        [Test]
        public void Show_EmptyProject_EmptyTask_ShouldDisplayNothing()
        {
            Console.SetIn(InputCommand("show","quit"));;
            
            Program.Main(new string[] { });

            ConsoleOutput().ShouldEqual("");
        }
        
        [Test]
        public void Show_HasProject_HasTask_ShouldDisplayTask()
        {
            var commands= InputCommand(
                "add project secrets",
                "add task secrets Eat more donuts.",
                "add task secrets Destroy all humans.",
                "show",
                "quit"
            );
            
            var expectedOutput = new []{
                "secrets",
                "    [ ] 1: Eat more donuts.",
                "    [ ] 2: Destroy all humans."
            };
            
            Console.SetIn(commands);

            Program.Main(new string[] { });
            
            ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine,expectedOutput));
        }
        
        [Test]
		public void Add_Project_ShouldDisplayEmptyProject()
		{
			var commands= InputCommand(
				"add project secrets",
								"show",
								"quit");
			
			Console.SetIn(commands);
            
			var expectedOutput = new []{
				"secrets"
			};
			
			Program.Main(new string[] { });
            
			ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine, expectedOutput));
		}

        [Test]
        public void Check_Task_ShouldDisplayCheckedTask()
        {
            var commands = InputCommand(
                "add project training",
                "add task training Four Elements of Simple Design",
                "add task training SOLID",
                "add task training Coupling and Cohesion",
                "add task training Primitive Obsession",
                "add task training Outside-In TDD",
                "add task training Interaction-Driven Design",
                "check 1",
                "check 3",
                "check 5",
                "check 6",
                "show",
                "quit");

            var expectedOutput = new[]
            {
                "training",
                "    [x] 1: Four Elements of Simple Design",
                "    [ ] 2: SOLID",
                "    [x] 3: Coupling and Cohesion",
                "    [ ] 4: Primitive Obsession",
                "    [x] 5: Outside-In TDD",
                "    [x] 6: Interaction-Driven Design"
            };

            Console.SetIn(commands);

            Program.Main(new string[] { });

            ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine, expectedOutput));
        }
        
        [Test]
		public void UnCheck_UncheckTask_ShouldDisplayUnCheckedTask()
		{
			var commands = InputCommand(
				"add project training",
				"add task training Four Elements of Simple Design",
				"add task training SOLID",
				"add task training Coupling and Cohesion",
				"add task training Primitive Obsession",
				"add task training Outside-In TDD",
				"add task training Interaction-Driven Design",
				"check 1",
				"check 3",
				"check 5",
				"check 6",
				"show",
				"uncheck 5",
				"uncheck 6",
				"show",
				"quit");
			
			var expectedOutput = new []{
				"training",
				"    [x] 1: Four Elements of Simple Design",
				"    [ ] 2: SOLID",
				"    [x] 3: Coupling and Cohesion",
				"    [ ] 4: Primitive Obsession",
				"    [x] 5: Outside-In TDD",
				"    [x] 6: Interaction-Driven Design",
				"",
				"training",
				"    [x] 1: Four Elements of Simple Design",
				"    [ ] 2: SOLID",
				"    [x] 3: Coupling and Cohesion",
				"    [ ] 4: Primitive Obsession",
				"    [ ] 5: Outside-In TDD",
				"    [ ] 6: Interaction-Driven Design"
			};
			
			Console.SetIn(commands);

			Program.Main(new string[] { });
            
			ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine,expectedOutput));
		}
		
		[Test]
		public void UnKnownCommand_ShouldDisplayUnknownErrorMessage()
		{
			var commands= InputCommand(
				"UnknownCommand",
				"quit");
			
			var expectedOutput = new[]{"I don't know what the command \"UnknownCommand\" is."};
			
			Console.SetIn(commands);

			Program.Main(new string[] { });
            
			ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine,expectedOutput));
		}
		
		[Test]
		public void UnKnownProject_ShouldDisplayUnknownProjectErrorMessage()
		{
			var commands= InputCommand(
				"add task unExistingProject onetask",
				"quit");
			
			var expectedOutput = new[]{"Could not find a project with the name \"unExistingProject\"."};
			
			Console.SetIn(commands);

			Program.Main(new string[] { });
            
			ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine,expectedOutput));
		}
		
		[Test]
		public void Check_UnKnownTask_ShouldDisplayUnknownTaskErrorMessage()
		{
			var commands= InputCommand(
				"add project secrets",
				"add task secrets Eat more donuts.",
				"check 5",
				"quit");
			
			var expectedOutput = new[]{"Could not find a task with an ID of 5."};
			
			Console.SetIn(commands);

			Program.Main(new string[] { });
            
			ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine,expectedOutput));
		}
		
		[Test]
		public void UnCheck_UnKnownTask_ShouldDisplayUnknownTaskErrorMessage()
		{
			var commands = InputCommand("add project secrets",
				"add task secrets Eat more donuts.",
				"check 1",
				"uncheck 5",
				"quit");
			
			var expectedOutput = new[]{"Could not find a task with an ID of 5."};
			
			Console.SetIn(commands);

			Program.Main(new string[] { });
            
			ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine,expectedOutput));
		}

		[Test]
		public void Help_ShouldDisplayAvailableCommand()
		{
			var commands = InputCommand("help","quit");
			var expectedOutput = new[]
			{
				"Commands:",
				"  show",
				"  add project <project name>",
				"  add task <project name> <task description>",
				"  check <task ID>",
				"  uncheck <task ID>"
			};
				
			Console.SetIn(commands);

			Program.Main(new string[] { });
            
			ConsoleOutput().ShouldEqual(string.Join(Environment.NewLine,expectedOutput));
		}

        private static StringReader InputCommand(params string[] commands)
        {
            return new StringReader(string.Join(Environment.NewLine, commands)); 
        }

        private string ConsoleOutput()
        {
            return RemovePromptCharacter(this.output.ToString());
        }

        private string RemovePromptCharacter(string outputString)
        {
            return outputString.Replace(">  ", "").TrimEnd(Environment.NewLine.ToCharArray());
        }
    }
}