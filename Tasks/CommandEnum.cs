﻿namespace Tasks
{
    public enum CommandEnum
    {
        show,
        add,
        check,
        uncheck,
        help,
        quit
    }
}