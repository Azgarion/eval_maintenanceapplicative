﻿using System;

namespace Tasks
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var taskList = new TaskManager();
            Run(taskList);
        }
        
        private static void Run(TaskManager taskManager)
        {
            while (true) {
                var command = TryParseInput(out var argument);
                if (command == CommandEnum.quit) break;
                Execute(taskManager, command, argument);
            }
        }
        
        private static CommandEnum TryParseInput(out string argument)
        {
            while (true)
            {
                Console.Write(">  ");
                var commandLine = Console.ReadLine();
                var command = SplitCommandAndArguments(commandLine, out argument);
                if (Enum.TryParse(command, out CommandEnum commandEnum))
                {
                    return commandEnum;
                }
                Error(command);
            }
        }

        private static int ParseCommandToInt(string argument)
        {
            int.TryParse(argument, out var taskIndex);
            return taskIndex;
        }
		
        private static void Execute(TaskManager taskManager, CommandEnum command, string arguments)
        {
            switch (command) {
                case CommandEnum.show:
                    taskManager.DisplayTaskForEachProject();
                    break;
                case CommandEnum.add:
                    Add(taskManager, arguments);
                    break;
                case CommandEnum.check:
                    taskManager.CheckTask(ParseCommandToInt(arguments));
                    break;
                case CommandEnum.uncheck:
                    taskManager.UncheckTask(ParseCommandToInt(arguments));
                    break;
                case CommandEnum.help:
                    PrintHelp();
                    break;
            }
        }
        private static void Error(string command)
        {
            Console.WriteLine("I don't know what the command \"{0}\" is.", command);
        }
        
        private static string SplitCommandAndArguments(string input, out string arguments)
        {
            if (string.IsNullOrEmpty(input))
            {
                arguments = "";
                return "";
            }

            var spaceIndex = input.TrimStart(' ').IndexOf(" ");
            if (spaceIndex < 0)
            {
                arguments = "";
                return input;
            }
			
            arguments = input.Substring(spaceIndex + 1);
            return input.Substring(0, spaceIndex);
        }
        
        private static void Add(TaskManager taskManager,string commandLine)
        {
            var subcommand = SplitCommandAndArguments(commandLine, out var arguments);

            if (subcommand == "project") {
                taskManager.AddProject(arguments);
            } 
			
            if (subcommand == "task")
            {
                var project = SplitCommandAndArguments(arguments, out var taskDescription);
                taskManager.AddTask(project, taskDescription);
            }
        }
        private static void PrintHelp()
        {
            Console.WriteLine("Commands:");
            Console.WriteLine("  show");
            Console.WriteLine("  add project <project name>");
            Console.WriteLine("  add task <project name> <task description>");
            Console.WriteLine("  check <task ID>");
            Console.WriteLine("  uncheck <task ID>");
            Console.WriteLine();
        }
        
    }
}