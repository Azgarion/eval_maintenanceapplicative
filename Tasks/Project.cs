﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    public class Project
    {
        private List<Task> taskList = new List<Task>();
        private string name;

        public Project(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return this.name;
        }
        
        public void AddTask(Task task)
        {
            this.taskList.Add(task);
        }
        
        public Task GetTask(int taskId)
        {
            return this.taskList.FirstOrDefault(task=> task.Id == taskId);
        }

        public override string ToString()
        {
            var taskString = string.Join(Environment.NewLine, this.taskList);
            return this.name+ Environment.NewLine+ (taskString ?? "");
        }

        public int CountTask()
        {
            return this.taskList.Count;
        }
    }
}