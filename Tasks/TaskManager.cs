﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
	public sealed class TaskManager
	{
		private readonly IList<Project> projects = new List<Project>();
		public void AddProject(string projectName)
		{
			this.projects.Add(new Project(projectName));
		}
		
		public void AddTask(string projectName, string taskDescription)
		{
			var project = this.GetProject(projectName);
			if (project == null)
			{
				Console.WriteLine("Could not find a project with the name \"{0}\".", projectName);
				return;
			}
			project.AddTask(new Task(){Id = NextId(), Description = taskDescription, Done = false});
		}
		
		public void DisplayTaskForEachProject()
		{
			foreach (var project in projects)
			{
				Console.WriteLine(project);
				Console.WriteLine();
			}
		}
		
		public void CheckTask(int id)
		{
			foreach (var project in projects)
			{
				var task = project.GetTask(id);
				if (task == null) continue;
				task.Done = true;
				return;
			}
			
			Console.WriteLine("Could not find a task with an ID of {0}.", id);
		}
		
		public void UncheckTask(int id)
		{
			foreach (var project in projects)
			{
				var task = project.GetTask(id);
				if (task == null) continue;
				task.Done = false;
				return;
			}
			Console.WriteLine("Could not find a task with an ID of {0}.", id);
		}

		private int NextId()
		{
			return projects.Sum(project => project.CountTask())+1;
		}
		
		private Project GetProject(string projectName)
		{
			return this.projects.FirstOrDefault(p => p.GetName() == projectName);
		}
	}
}
